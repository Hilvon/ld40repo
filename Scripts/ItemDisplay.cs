﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemDisplay<T> : MonoBehaviour where T:class {
    protected T displayedItem { get; private set; }
    public void DisplayItem(T Data) {
        if (Data!=displayedItem) {
            if (displayedItem != null && displayedItem is IMutable) ((IMutable)displayedItem).OnChanged -= UpdateDetails;
            displayedItem = Data;
            if (displayedItem != null && displayedItem is IMutable) ((IMutable)displayedItem).OnChanged -= UpdateDetails;
        }
        UpdateDetails();
    }
    public void UpdateDetails() {
        foreach (IItemDetails Det in GetComponentsInChildren<IItemDetails>(true)) {
            Det.Show(displayedItem);
        }        
    }    

    public interface IItemDetails {
        void Show(T Data);
    }
}
public interface IMutable {
    event System.Action OnChanged;
}


public abstract class SubDisplay<T,D>: ItemDisplay<D>, ItemDisplay<T>.IItemDetails where T: class where D:class {
    protected abstract D getData(T Data);
    public void Show(T Data) {
        D tmp = getData(Data);
        gameObject.SetActive(tmp != null);
        if (tmp!=null) DisplayItem(tmp);
    }
}

public abstract class ModeDisplay<T, D> : SubDisplay<T, D> where T : class where D : class, T {
    protected override D getData(T Data) {
        return Data as D;
    }
}

public abstract class SwitchDisplay<T>: MonoBehaviour, ItemDisplay<T>.IItemDetails where T:class {
    public void Show(T Data) {
        gameObject.SetActive(IsEnabled(Data));
    }
    protected abstract bool IsEnabled(T Data);
}
