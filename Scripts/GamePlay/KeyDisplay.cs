﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDisplay : ItemDisplay<KeyDisplay.KyeDisplayData>  {
    public KeyDisplayToken Prefab;
    static System.Action OnTotalCahnged;
    public static void PrepareDisplay() {
        if (OnTotalCahnged != null) OnTotalCahnged();
    }
    // Use this for initialization
    void Start () {
        OnTotalCahnged += SpawnTokens;
        KeyGem.OnCountChanges += UpdateDisplay;
    }
	
    void SpawnTokens() {
        int NewTotal = KeyGem.TotalKeys;
        List<KeyDisplayToken> Tokens = new List<KeyDisplayToken>(GetComponentsInChildren<KeyDisplayToken>());
        while (Tokens.Count > NewTotal) {
            KeyDisplayToken TT = Tokens[0];
            Tokens.Remove(TT);
            Destroy(TT.gameObject);            
        }
        while (Tokens.Count<NewTotal) {
            KeyDisplayToken TT = Instantiate(Prefab, transform, false);
            Tokens.Add(TT);
        }

        for (int i = 0; i < NewTotal; i++) {
            KeyDisplayToken TT = Tokens[i];
            TT.Number = i + 1;
            TT.transform.SetAsLastSibling();
        }
    }
    void UpdateDisplay() {
        this.DisplayItem(new KyeDisplayData() { Count = KeyGem.KeysCollected });
    }

	// Update is called once per frame
	void Update () {
		
	}
    public class KyeDisplayData {
        public int Count;
    }
}
