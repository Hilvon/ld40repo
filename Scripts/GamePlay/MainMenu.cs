﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    bool IsGamePlaying;
    public UnityEngine.UI.Button ContinueBtn;
    public UnityEngine.UI.Button ReStartBtn;
    public UnityEngine.UI.Button StartBtn;
    public LoadingScreen LoadingScreen;
    public GameObject Congratulations;
    public ConstructionScript Constructor;

    public static MainMenu Main;
    // Use this for initialization
    void Start () {
        Main = this;
        StartMenuMode();
        Congratulations.SetActive(false);

        ContinueBtn.gameObject.SetActive(false);
        ReStartBtn.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void StartPlayMode() {
        gameObject.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState =  CursorLockMode.Locked;
    }

    public void StartMenuMode() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        gameObject.SetActive(true);
    }

    ConstructionScript.DungeonInfo Dungeon;
    public Texture2D[] Maps;
    public void StartGame() {
        StartPlayMode();
        LoadingScreen.StartLoading(DungeonMapper.Instance.BuildDungeon(Maps[Random.Range(0, Maps.Length)], Random.Range(0, 1000), (D) => {
            Dungeon = D;
            Constructor.DrawDungeon(D);
            ContinueBtn.gameObject.SetActive(true);
            ReStartBtn.gameObject.SetActive(true);            
        }));
    }

    public void RestartGame() {
        Congratulations.SetActive(false);
        Constructor.DrawDungeon(Dungeon);
        ContinueBtn.gameObject.SetActive(true);
        StartPlayMode();
    }

    public void WinGame() {
        Congratulations.SetActive(true);
        ContinueBtn.gameObject.SetActive(false);
        StartMenuMode();        
    }

    public void ExitGame() {
        Application.Quit();
    }
}
