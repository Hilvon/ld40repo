﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltarEffect : SwitchDisplay<Altar.AltarData>   {
    public float Thresholt;

    protected override bool IsEnabled(Altar.AltarData Data) {
        if (Thresholt<0) {
            return Data.progress + Thresholt > 0;
        }
        else {
            return Data.progress <= Thresholt;
        }
    }
}
