﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTokenMode : SwitchDisplay<KeyDisplayToken.TokenData> {
    public bool MatchedMode;

    protected override bool IsEnabled(KeyDisplayToken.TokenData Data) {
        return Data.IsCollected==MatchedMode;
    }
}
