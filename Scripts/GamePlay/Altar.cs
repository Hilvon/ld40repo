﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Altar : ItemDisplay<Altar.AltarData> {

	// Use this for initialization
	void Start () {
        KeyGem.OnCountChanges += UpdateAltar;

    }
	
    void UpdateAltar() {
        this.DisplayItem(new AltarData() { progress = (float)KeyGem.KeysCollected / KeyGem.TotalKeys });
    }
	// Update is called once per frame
	void Update () {
		
	}
    
    public class AltarData {
        public float progress;
    }

    private void OnTriggerEnter(Collider other) {
        Debug.Log("We have collision!");
        Player P = other.GetComponentInParent<Player>();
        if (P != null) {
            if (KeyGem.KeysCollected==KeyGem.TotalKeys) {
                MainMenu.Main.WinGame();
            }
            //Destroy(gameObject);
            //KeysCollected++;
            //TriggerChage();
        }
    }
}
