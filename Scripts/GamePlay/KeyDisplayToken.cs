﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDisplayToken : SubDisplay<KeyDisplay.KyeDisplayData, KeyDisplayToken.TokenData> {
    public int Number;
    protected override TokenData getData(KeyDisplay.KyeDisplayData Data) {
        return new TokenData() { IsCollected = Data.Count >= Number };
    }
    
    public class TokenData {
        public bool IsCollected;
    }
}
