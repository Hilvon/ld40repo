﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyGem : MonoBehaviour {
    public static int KeysCollected { get; private set; }
    public static event System.Action OnCountChanges;
    public static int TotalKeys { get; private set; }

    public static void ResetKeys(int Total) {
        TotalKeys = Total;
        KeysCollected = 0;
        TriggerChage();
    }

    public Transform Graphic;

    public float bobspeed;
    public float rotSpeed;
    public float bobAmp;
    // Use this for initialization
	void Start () {
        t = Random.Range(0, 1f);
	}
    float t;
	// Update is called once per frame
	void Update () {
        t += Time.deltaTime;
        Graphic.localPosition = new Vector3(0, bobAmp * Mathf.Sin(t * bobspeed), 0);
        Graphic.rotation = Quaternion.Euler(0, t * rotSpeed, 0);
	}

    private void OnTriggerEnter(Collider other) {
        Debug.Log("We have collision!");
        Player P = other.GetComponentInParent<Player>();
        if (P != null) {
            Destroy(gameObject);
            KeysCollected++;
            TriggerChage();
        }
    }

    static void TriggerChage() {
        if (OnCountChanges != null) OnCountChanges();
    }
}
