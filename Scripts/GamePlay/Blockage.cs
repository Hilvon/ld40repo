﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blockage :  MonoBehaviour, ItemDisplay<Tile>.IItemDetails {
    public void Show(Tile Data) {
        myTrigger = Data.Blockage;
        if (myTrigger < 99) {
            KeyGem.OnCountChanges += CheckTrigger;            
        }
        gameObject.SetActive(false);
    }
    int myTrigger;
    void CheckTrigger() {
        if (KeyGem.KeysCollected >=myTrigger) {
            gameObject.SetActive(true);
            KeyGem.OnCountChanges -= CheckTrigger;
        }
    }    
}
