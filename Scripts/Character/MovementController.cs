﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

    CharacterController _myRB;
    CharacterController myRB {
        get {
            if (_myRB == null) _myRB = GetComponent<CharacterController>();
            return _myRB;
        }
    }

    public Transform PitchControl;

	// Use this for initialization
	void Start () {
        PrevMousePos = Input.mousePosition;

    }
    public Vector3 curVelocity;
    Vector3 PrevMousePos;
    // Update is called once per frame
	void Update () {
        Vector3 wantedVel = transform.rotation*new Vector3(Input.GetAxis("Horizontal"), 0,Input.GetAxis("Vertical")) *5;
        float Acc = Input.GetAxis("Horizontal") >= 0 ? 1 : 3;
        curVelocity = Vector3.Lerp(curVelocity, wantedVel, Time.deltaTime* Acc);
        myRB.SimpleMove(curVelocity);
        Vector3 MoseOffset = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));//   Input.mousePosition - PrevMousePos;
        PrevMousePos = Input.mousePosition;

        transform.Rotate(Vector3.up, MoseOffset.x);
        PitchControl.Rotate(Vector3.left, MoseOffset.y);

        if (Input.GetKey(KeyCode.Escape)) {
            MainMenu.Main.StartMenuMode();
        }
    }
}
