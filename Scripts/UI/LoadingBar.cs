﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBar : MonoBehaviour, LoadingScreen.IItemDetails {
    public RectTransform Bar;

    public void Show(ThreadMaster.ThreadSynk Data) {
        Debug.Log(Data.Progress);
        Bar.anchorMax =new Vector2(Data.Progress, 1);
        //Bar.localPosition = Vector3.zero;
    }
}
