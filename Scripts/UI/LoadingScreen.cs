﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : ItemDisplay<ThreadMaster.ThreadSynk> {
    public CanvasGroup Group;
    float DesiredOpacity = 0;
	// Update is called once per frame
    public void StartLoading(ThreadMaster.ThreadSynk Data) {
        DesiredOpacity = 1;
        DisplayItem(Data);
        gameObject.SetActive(true);
    }

	void Update () {
		if (displayedItem!= null && displayedItem.isReady == false) {
            gameObject.SetActive(true);
            UpdateDetails();
        } else {
            DesiredOpacity = 0;
        }

        if (Group.alpha!= DesiredOpacity) {
            if (Mathf.Abs(Group.alpha - DesiredOpacity) < 0.1) {
                Group.alpha = DesiredOpacity;
                gameObject.SetActive(DesiredOpacity > 0);
                Debug.Log("Loading screen done: " + DesiredOpacity);
            }
            else {
                Group.alpha = Mathf.Lerp(Group.alpha, DesiredOpacity, 2 * Time.unscaledDeltaTime);
            }
        }
	}
}
