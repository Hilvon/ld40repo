﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreadMaster : MonoBehaviour {
    static ThreadMaster Main;
    public static ThreadSynk Offload( System.Action<ThreadSynk> Calculations, System.Action OnComplete) {
        ThreadSynk Sync = new ThreadSynk() { Callback = OnComplete, isReady = false, Progress = 0 };
        Main.Synks.Add(Sync);
        new System.Threading.Thread(() => {
            Calculations(Sync);
            Sync.isReady = true;
        }).Start();
        return Sync;
    }
	// Use this for initialization
	void Start () {
        Main = this;
	}
    private void OnDestroy() {
        Main = null;
    }


    public class ThreadSynk {
        public bool isReady;
        public float Progress;
        public string Label;
        public System.Action Callback;
    }

    List<ThreadSynk> Synks = new List<ThreadSynk>();
    // Update is called once per frame
    void Update() {
        if (Synks.Count > 0) {
            ThreadSynk[] tmp = Synks.ToArray();
            foreach (ThreadSynk S in tmp) {
                if (S.isReady) {
                    if (S.Callback != null) S.Callback();
                    Synks.Remove(S);
                }
            }
        }
    }
}
