﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile {
    public Tile Up, Left, Right, Down;
    public TileGroup Group;
    public int GroupType;
    public int TileMode;
    public int X, Y;
    public int NeighborsCount {
        get {
            if (neighbors == null) {
                Recount();
            }
            return neighbors.Count;
        }
    }
    void Recount() {
        neighbors = new List<Tile>();
        if (Up != null) neighbors.Add(Up);
        if (Down != null) neighbors.Add(Down);
        if (Left != null) neighbors.Add(Left);
        if (Right != null) neighbors.Add(Right);
    }
    List<Tile> neighbors;
    public Tile[] Neighbors {
        get {
            if (neighbors == null) {
                Recount();
            }
            return neighbors.ToArray();
        }
    }
    public int Blockage = 99;

    public class TileGroup {
        List<Tile> tiles = new List<Tile>();
        public Tile[] Tiles { get { return tiles.ToArray(); } }
        public bool IsInside(Tile T) {
            return tiles.Contains(T);
        }
        public void AddTile(Tile T) {
            tiles.Add(T);
            T.Group = this;
        }
        public Tile PickRandomTile(System.Random RNG) {
            return tiles[RNG.Next(tiles.Count)];
        }
    }
    
}
