﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : SubDisplay<Tile,Wall.WallInfo> {
    public enum Dir { Up,Left,Down,Right}
    public Dir Direction;
    protected override WallInfo getData(Tile Data) {
        switch (Direction) {
            case Dir.Up: return CheckNeighbor(Data.Up);
            case Dir.Right: return CheckNeighbor(Data.Right);
            case Dir.Left: return CheckNeighbor(Data.Left);
            case Dir.Down: return CheckNeighbor(Data.Down);
        }
        return null;
    }

    WallInfo CheckNeighbor(Tile tile) {
        if (tile == null) return new WallInfo();
        else return null;
    }

    public enum Dirs { Up,Down,Left, Right}
    Vector3 Facing() {
        switch (Direction) {
            case Dir.Up: return Vector3.forward;
            case Dir.Down: return Vector3.back;
            case Dir.Left: return Vector3.right;
            case Dir.Right: return Vector3.left;
            default: return Vector3.forward;
        }
    }
    

    public class WallInfo {

    }
    Renderer[]  _Renderer;
    public Transform C1, C2;
    Renderer[] Graphics { get {
            if (_Renderer == null) _Renderer =  GetComponentsInChildren<Renderer>();
            return _Renderer;
        } }
    private void Update() {
        bool B1 = C1 == null ? false : Vector3.Dot(Facing(), C1.position - Camera.main.transform.position) <= 0;
        bool B2 = C2 == null ? false : Vector3.Dot(Facing(), C2.position - Camera.main.transform.position) <= 0;
        bool B3 = Vector3.Dot(Facing(), transform.position - Camera.main.transform.position) <= 0;

        foreach (Renderer R in Graphics) {
            R.enabled = B1 || B2 || B3;
        }
        
        //Graphics.SetActive(Vector3.Dot(Facing(), Camera.main.transform.forward) <= 0);
    }
}
