﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorGraphig : MonoBehaviour, ItemDisplay<Tile>.IItemDetails {
    public void Show(Tile Data) {

        GetComponent<MeshRenderer>().material.color = GetColor(Data); // Data.GroupType == 1 ? Color.blue : Color.green;

    }

    Color GetColor(Tile Data) {
        //switch (Data.Neighbors) {
        //    case 1: return Color.red;
        //    case 2: return Color.blue;
        //    default: return Color.green;
        //}
        //if ((Data.Neighbors & 8) != 0) return Color.green;
        //return Color.red;
        //if (Data.Blockage < 99) return Color.red;
        //if (Data.Group is DungeonMapper.Room) return Color.blue;
        //if (Data.Group is DungeonMapper.Passage) return Color.magenta;
        //else return Color.green;
        return Color.gray;
    }
}
