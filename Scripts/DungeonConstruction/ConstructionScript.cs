﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ConstructionScript : MonoBehaviour {
    public Floor FloorPrefab;
    public Wall WallPrefab;
    public Player Player;
    public KeyGem KeyPrefab;
    public Altar AltarPrefab;

    float scale { get { return FloorPrefab.Scale; } }




    //void ListTiles(Texture2D Map) {
    //    int Width = Map.width - 1;
    //    int Height = Map.height - 1;
    //    Tiles = new Tile[Width, Height];
    //    AllTiles = new List<Tile>();
    //    Altars = new List<Tile>();
    //    Spawns = new List<Tile>();
    //    Keys = new List<Tile>();
    //    //Rad texture into map;
    //    for (int i = 1; i < Width; i++) {
    //        for (int j = 1; j < Height; j++) {
    //            Color S = Map.GetPixel(i, j);
    //            if (S != BGKey) {
    //                Tile curTile = new Tile() {
    //                    Up = Tiles[i, j - 1],
    //                    Left = Tiles[i - 1, j],
    //                    TileMode = (S == GemKey) ? 1 : (S == AltarKey) ? 2 : (S == StartKey) ? 3 : 0,
    //                    X = i,
    //                    Y = j
    //                };
    //                if (curTile.TileMode == 1) Keys.Add(curTile);
    //                if (curTile.TileMode == 2) Altars.Add(curTile);
    //                if (curTile.TileMode == 3) Spawns.Add(curTile);

    //                if (curTile.Left != null) curTile.Left.Right = curTile;
    //                if (curTile.Up != null) curTile.Up.Down = curTile;
    //                Tiles[i, j] = curTile;
    //                AllTiles.Add(curTile);
    //            }
    //        }
    //    }
    //}

    //Tile[,] Tiles;
    //List<Tile> AllTiles;
    //List<Tile> Altars;
    //List<Tile> Spawns;
    //List<Tile> Keys;

    //RoomManager RM;


    //List<Room> Halls;
    //List<Passage> Passages;

    //public DungeonInfo CreateDUngeon(Texture2D Map, int Seed) {
    //    GenerateGungeon(Map, Seed);
    //    return new DungeonInfo() {
    //        Tiles = AllTiles, AltarTile= 

    //    }
    //}


    //DungeonInfo GenerateGungeon(Texture2D Map, int Seed) {

    //    Tile AltarTile =null, SpawnTile=null;
    //    ListTiles(Map);

    //    ThreadSynk TS = new ThreadSynk() { Callback = () => { DrawDungeon(AllTiles, SpawnTile, AltarTile, Keys); } };
    //    Synks.Add(TS);
    //    new System.Threading.Thread(() => {

    //        RM = new RoomManager(AllTiles);

    //        System.Random RNG = new System.Random(Seed);

    //        //Now we need to pick an Altar;
    //        AltarTile = Altars[RNG.Next(Altars.Count)];
    //        foreach (Tile T in Altars) {
    //            if (T != AltarTile) T.TileMode = 0;
    //        }

    //        SpawnTile = Spawns[RNG.Next(Spawns.Count)];
    //        foreach (Tile T in Spawns) {
    //            if (T != SpawnTile) T.TileMode = 0;
    //        }

    //        Debug.Log("Starting Route calculation:");
    //        RouteCollection Routes = RM.BuildAllRoutes((Room)AltarTile.Group);
    //        Debug.Log(Routes.Routes.Count + ": " + Routes.NonEssentials());
    //        while (Routes.NonEssentials()>0) {
    //        //for (int i = 0; i < 10; i++) {

    //            Passage Rem = Routes.PickNonEssential(RNG);
    //            Tile Block = Rem.PickRandomTile(RNG);
    //            Block.Blockage = RNG.Next(Keys.Count+1);
    //            Routes.ExcludePassage(Rem);
    //            Debug.Log(Routes.Routes.Count+": "+Routes.NonEssentials());
    //        }
    //        TS.isReady = true;

    //    }).Start();



    //    //Display everything!
    //    //for (int i = 1; i < Width; i++) {
    //    //    for (int j = 1; j < Height; j++) {
    //    //        if (Tiles[i,j]!=null) {
    //    //            Tile T = Tiles[i, j];




    //    //        }
    //    //    }
    //    //}
    //}
    List<Floor> PlacedTiles = new List<Floor>();

    public void DrawDungeon( DungeonInfo Info ) {
        DrawDungeon(Info.Tiles, Info.SpawnTile, Info.AltarTile, Info.KeyTiles);
    }
    void DrawDungeon(IEnumerable<Tile> AllTiles, Tile SpawnTile, Tile AltarTile, Tile[] KeyTiles) {
        ClearLevel();
        Debug.Log(AllTiles + "; " + SpawnTile + "; " + AltarTile + "; " + KeyTiles);
        KeyGem.ResetKeys(KeyTiles.Length);
        KeyDisplay.PrepareDisplay();
        foreach (Tile T in AllTiles) {
            if (T.TileMode != -1) {
                Floor curF = Instantiate(FloorPrefab, new Vector3(T.X, 0, T.Y) * scale, Quaternion.identity, transform);
                curF.DisplayItem(T);
                if (T == SpawnTile)
                    Player.transform.position = curF.transform.position;
                if (T == AltarTile) {
                    Altar A = Instantiate(AltarPrefab, curF.transform.position, Quaternion.identity, transform);
                }
                if (KeyTiles.Contains(T)) {
                    KeyGem.Instantiate(KeyPrefab, curF.transform.position, Quaternion.identity, transform);
                }
                PlacedTiles.Add(curF);
            }
        }
    }

    void ClearLevel() {
        foreach(Floor F in PlacedTiles) {
            Destroy(F.gameObject);
        }
        PlacedTiles.Clear();
    }



    //Tile PickAltarTile(List<Tile> Altars, List<Node> Nodes, System.Random RNG) {
    //    if (Altars.Count>0) {
    //        return Altars[RNG.Next(Altars.Count)];
    //    }
    //    else { //No altars marked on the layout. Nominate a random non-important Hall.
    //        List<Node> Candidates = new List<Node>();
    //        foreach (Node N in Nodes) { if (N.IsImportat == false) Candidates.Add(N); }
    //        if (Candidates.Count > 0) {
    //            return Candidates[RNG.Next(Candidates.Count)].PickRandomTile(RNG);
    //        }
    //        else {
    //            Tile tmp = 
    //        }
    //    }
    //}
    //Tile[] GetTileNeighbors(Tile T) {
    //    List<Tile> res = new List<Tile>();
    //    if (T.Up != null) res.Add(T.Up);
    //    if (T.Down != null) res.Add(T.Down);
    //    if (T.Left != null) res.Add(T.Left);
    //    if (T.Right != null) res.Add(T.Right);
    //    return res.ToArray();
    //}











    





    public Texture2D testMap;

    // Use this for initialization
    void Start() {
        //GenerateGungeon(testMap, 3);

    }
    

    //public class Room : Tile.TileGroup {
    //    //public bool IsImportat;
    //    public Tile[] FreeTiles() {
    //        return Tiles.Where((T) => { return T.TileMode == 0; }).ToArray();
    //    }
    //    public bool ConaisAltar() {
    //        return Tiles.Where((T) => { return T.TileMode == 2; }).Count() > 0;
    //    }
    //    public bool ConaisGem() {
    //        return Tiles.Where((T) => { return T.TileMode == 1; }).Count() > 0;
    //    }
    //    public bool ConaisStart() {
    //        return Tiles.Where((T) => { return T.TileMode == 3; }).Count() > 0;
    //    }
    //    public bool ConaisSpecial() {
    //        return Tiles.Where((T) => { return T.TileMode != 0; }).Count() > 0;
    //    }
    //}
    //public class Passage : Tile.TileGroup {
    //    public Room From;
    //    public Room To;

    //}
    //class RoomManager {
    //    public Room[] Halls;
    //    public Passage[] Passages;

    //    public RoomManager(List<Tile> TileSet) {
    //        List<Tile> Type1 = new List<Tile>();
    //        List<Tile> Type0 = new List<Tile>();
    //        foreach (Tile T in TileSet) {
    //            if (T.NeighborsCount > 2 || T.NeighborsCount == 1 || T.TileMode != 0) {
    //                Type1.Add(T);
    //                T.GroupType = 1;
    //            }
    //            else if (T.NeighborsCount == 0) T.GroupType = -1;
    //            else {
    //                T.GroupType = 0;
    //                Type0.Add(T);
    //            }
    //        }

    //        List<Room> halls = new List<Room>();

    //        while (Type1.Count > 0) {
    //            halls.Add(AssignHall(Type1[0], Type1));
    //        }

    //        foreach (Tile T in Type0) {
    //            if (IsHallCorner(T)) Type1.Add(T);
    //        }
    //        //Removing Hall corners from Corridor lists;
    //        foreach (Tile T in Type1) {
    //            Type0.Remove(T);
    //        }

    //        List<Passage> passages = new List<Passage>();

    //        //Assigning Passages to groups;
    //        while (Type0.Count > 0) {
    //            passages.Add(AssignCorridor(Type0[0], Type0));
    //        }

    //        Halls = halls.ToArray();
    //        Passages = passages.ToArray();
    //    }

        //public RouteCollection BuildAllRoutes(Room Start) {
        //    List<Route> Routes = new List<Route>();
        //    List<Route> PrevAdded;
        //    List<Route> NewlyAdded = new List<Route>();
        //    //Dictionary<Room, List<Route>> Dictionary = new Dictionary<Room, List<Route>>();
        //    Route Root = new Route() { Passages = new List<Passage>(), VisitedNodes = new List<Room>() { Start }, FinalDest = Start };
        //    //int newlyAdded = 0;
        //    foreach (Passage P in Passages) {
        //        Route New = Root.AppendPassage(P);
        //        if (New != null) {
        //            //Routes.Add(New);
        //            //newlyAdded++;
        //            NewlyAdded.Add(New);
        //        }
        //    }
        //    //Debug.Log(newlyAdded);
        //    while (NewlyAdded.Count != 0) {
        //    //for (int i = 0; i < 25; i++) {
        //        PrevAdded = NewlyAdded;
        //        NewlyAdded = new List<Route>();
        //        foreach (Route R in PrevAdded) {
        //            if (R.FinalDest.ConaisSpecial()) {
        //                Routes.Add(R);
        //            }
        //        }
        //        //Debug.Log(Passages.Length);
        //        //Route[] curRoutes = Routes.ToArray();
        //        foreach (Route R in PrevAdded) {
        //            foreach (Passage P in Passages) {
        //                Route New = R.AppendPassage(P);
        //                if (New != null) {
        //                    //Routes.Add(New);
        //                    //newlyAdded++;
        //                    NewlyAdded.Add(New);
        //                }
        //                else {
        //                    //Debug.Log("!!!");
        //                }
        //            }
        //        }
        //        Debug.Log(NewlyAdded.Count);
        //    }
        //    return new RouteCollection(Routes);
        //}



        //Room AssignHall(Tile T, List<Tile> Exclude) {
        //    Room myHall = new Room();
        //    ExpandGroup(T, myHall, 1, Exclude);
        //    //myHall.IsImportat = false;
        //    //foreach (Tile I in myHall.Tiles) {
        //    //    if (I.TileMode != 0) myHall.IsImportat = true;
        //    //}
        //    return myHall;
        //}

        //Passage AssignCorridor(Tile T, List<Tile> Exclude) {
        //    Passage myPassage = new Passage();
        //    ExpandGroup(T, myPassage, 0, Exclude);

        //    List<Room> Conections = new List<Room>();
        //    //Debug.Log("Passage contains tiles: "+myPassage.Tiles.Length);
        //    foreach (Tile I in myPassage.Tiles) {
        //        if (I.Up != null && I.Up.GroupType != 0) Conections.Add((Room)I.Up.Group);
        //        if (I.Down != null && I.Down.GroupType != 0) Conections.Add((Room)I.Down.Group);
        //        if (I.Left != null && I.Left.GroupType != 0) Conections.Add((Room)I.Left.Group);
        //        if (I.Right != null && I.Right.GroupType != 0) Conections.Add((Room)I.Right.Group);
        //    }
        //    if (Conections.Count == 0) {
        //        //Debug.Log("Loop Located");
        //    }
        //    else if (Conections.Count != 2) {
        //        Debug.LogError("Something hairy going on...");
        //    }
        //    else {
        //        myPassage.From = Conections[0];
        //        myPassage.To = Conections[1];
        //    }
        //    return myPassage;
        //}
        //void ExpandGroup(Tile T, Tile.TileGroup G, int GroupType, List<Tile> Exclude) {
        //    //if (GroupType ==0) { Debug.Log("Expanding Corridor to : " + T.X + "x" + T.Y + " (" + T.GroupType + ")"); }
        //    if (T.Group != null) return; // Already in group!
        //    if (T.GroupType != GroupType) return; //Incompatible group type!
        //    G.AddTile(T);//
        //    Exclude.Remove(T);
        //    if (T.Up != null) ExpandGroup(T.Up, G, GroupType, Exclude);
        //    if (T.Down != null) ExpandGroup(T.Down, G, GroupType, Exclude);
        //    if (T.Left != null) ExpandGroup(T.Left, G, GroupType, Exclude);
        //    if (T.Right != null) ExpandGroup(T.Right, G, GroupType, Exclude);
        //}
        //bool IsHallCorner(Tile T) {
        //    Tile N1 = T.Up != null ? T.Up : T.Down != null ? T.Down : T.Left;
        //    Tile N2 = T.Right != null ? T.Right : T.Left != null ? T.Left : T.Down;
        //    if (N1 == null || N2 == null) return false; //Should neve occure in normal execution
        //    if (N1.Group != null && N1.Group == N2.Group) {
        //        T.Group = N1.Group;
        //        T.GroupType = 1;
        //        return true;
        //    }
        //    return false;
        //}
    //}
    //class RouteCollection {
    //    //public Room[] ReachableRooms;
    //    public List<Passage> UsedPassages;
    //    public Passage[] Mandatory;
    //    public Room[] Important;
    //    public List<Route> Routes;
    //    public RouteCollection(IEnumerable<Route> Routes) {
    //        //List<Route> toPurge = new List<Route>();
    //        //foreach (Route R in Routes) {
    //        //    if (R.FinalDest.ConaisSpecial()==false) {
    //        //        toPurge.Add(R);
    //        //    }
    //        //}
            


    //        this.Routes = new List<Route>(Routes);
    //    //    Recount();
    //    //}

    //    //void Recount() {
    //        List<Passage> passages = new List<Passage>();
    //        //List<Room> rooms = new List<Room>();
    //        List<Room> imps = new List<Room>();

    //        foreach (Route R in Routes) {
    //            //if (rooms.Contains(R.FinalDest) == false) {
    //                Room Ro = R.FinalDest;
    //            //    rooms.Add(Ro);
    //                if (imps.Contains(Ro)==false) imps.Add(Ro);
    //            //}
    //            foreach (Passage P in R.Passages) {
    //                if (passages.Contains(P) == false) passages.Add(P);
    //            }
                
    //        }
    //        //ReachableRooms = rooms.ToArray();
    //        UsedPassages = passages;//.ToArray();
    //        Important = imps.ToArray();
    //        CountMandatory();
    //    }
    //    void CountMandatory() {
    //        List<Passage> man = new List<Passage>();
    //        foreach (Room Ro in Important) {
    //            List < Passage > tmp = new List<Passage>(UsedPassages.Except(man));
    //            Route[] Rs = Routes.Where((R) => { return R.FinalDest == Ro; }).ToArray();
    //            foreach (Route R in Rs) {
    //                IEnumerable<Passage> missing = tmp.Except(R.Passages).ToArray();
    //                foreach (Passage P in missing) tmp.Remove(P);
    //            }
    //            foreach (Passage P in tmp) {
    //                man.Add(P);
    //            }
    //        }
    //        Mandatory = man.ToArray();
    //    }
    //    public int NonEssentials() {
    //        return UsedPassages.Except(Mandatory).Count();
    //    }
    //    public Passage PickNonEssential(System.Random RNG) {
    //        Passage[] NonEssentials = UsedPassages.Except(Mandatory).ToArray();
    //        return NonEssentials[RNG.Next(NonEssentials.Length)];
    //    }
    //    public void ExcludePassage(Passage P) {
    //        IEnumerable<Route> Removals = Routes.Where((R) => { return R.Passages.Contains(P); }).ToArray ();
    //        foreach (Route R in Removals) {
    //            Routes.Remove(R);
    //        }
    //        UsedPassages.Remove(P);
    //        CountMandatory();
    //    }
    //}

    


    public class DungeonInfo {
        public Tile[] Tiles;
        public Tile SpawnTile;
        public Tile AltarTile;
        public Tile[] KeyTiles;
    }
}

//int CheckNeighbor(Color N, Quaternion Rot, Floor Base) {
//    if (N != BGKey) return 1;
//    Wall curWall = Instantiate(WallPrefab, Base.transform);
//    curWall.transform.rotation = Rot;
//    //curWall.transform.localPosition = Rot * Vector3.back * scale / 2;
//    return 0;
//}

//public void GenerateGungeonLegacy(Texture2D Map, int Seed) {
//    int Width = Map.width-1;
//    int Height = Map.height-1;
//    Debug.Log(Width+";"+Height);
//    System.Random RNG = new System.Random(Seed);

//    List<Floor> OneExit = new List<Floor>();
//    List<Floor> TwoExit = new List<Floor>();
//    List<Floor> MulExit = new List<Floor>();
//    List<Floor> StartPoints = new List<Floor>();

//    List<Floor> Important = new List<Floor>();

//    for (int i = 1; i < Width; i++) {
//        for (int j = 1; j < Height; j++) {
//            Color S = Map.GetPixel(i, j);
//            if (S != BGKey) {
//                Debug.Log(S);
//                //It's not a void space! 
//                Floor curFloor = Instantiate(FloorPrefab, new Vector3(i, 0,j)*scale,Quaternion.identity, transform);
//                int Passages = 0;
//                Passages += CheckNeighbor(Map.GetPixel(i - 1, j), Quaternion.Euler(0, 90, 0), curFloor);
//                Passages += CheckNeighbor(Map.GetPixel(i + 1, j), Quaternion.Euler(0, 270, 0), curFloor);
//                Passages += CheckNeighbor(Map.GetPixel(i, j - 1), Quaternion.Euler(0, 0, 0), curFloor);
//                Passages += CheckNeighbor(Map.GetPixel(i , j + 1), Quaternion.Euler(0, 180, 0), curFloor);

//                if (Passages==1) {
//                    OneExit.Add(curFloor);
//                } else if(Passages == 2) {
//                    TwoExit.Add(curFloor);
//                } else {
//                    MulExit.Add(curFloor);
//                }

//                //Color NL = Map.GetPixel(i-1, j);
//                //Color NU = Map.GetPixel(i - 1, j);
//                //Color NR = Map.GetPixel(i - 1, j);
//                //Color NB = Map.GetPixel(i - 1, j);
//                if (S == StartKey) StartPoints.Add(curFloor);

//                if (S == GemKey) {
//                    Important.Add(curFloor);
//                } 
//            }
//        }
//    }
//    Dictionary<Floor, Tile> dict = new Dictionary<Floor, Tile>();        

//    foreach (Floor F in MulExit) {

//    }



//    Floor StartTile = StartPoints[RNG.Next(StartPoints.Count)];
//    Player.transform.position = StartTile.transform.position;
//}