﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[CreateAssetMenu(fileName = "Mapper", menuName = "Custom/MazeMapper")]
public class DungeonMapper : ScriptableObject {
    public static DungeonMapper Instance {
        get {
            DungeonMapper res = Resources.LoadAll<DungeonMapper>("").FirstOrDefault();
            if (res == null) res = CreateInstance<DungeonMapper>();
            return res;
        }
    }

    public Color BGKey = Color.white;
    public Color GemKey = Color.red;
    public Color StartKey = Color.green;
    public Color AltarKey = Color.blue;

    public ThreadMaster.ThreadSynk BuildDungeon(Texture2D Map, int Seed, System.Action<ConstructionScript.DungeonInfo> Callback) {
        ConstructionScript.DungeonInfo res = new ConstructionScript.DungeonInfo();

        System.Random RNG = new System.Random(Seed);

        TileMap MT = new TileMap(Map, BGKey, GemKey, AltarKey, StartKey);
        ThreadMaster.ThreadSynk Sync = ThreadMaster.Offload((TS) => {

            TS.Label = "Detecting rooms and passages";
            TS.Progress = 0;
            RoomManager RM = new RoomManager(MT.AllTiles);

            if (MT.Keys.Count == 0) {
                //NO KEY ROOMS SPECIFIED!
                Room[] tmp = RM.Halls.Where((H) => { return RNG.Next(100) >= 50; }).ToArray();
                foreach (Room R in tmp) {
                    Tile[] free = R.FreeTiles();
                    Tile newKey = free[RNG.Next(free.Length)];
                    newKey.TileMode = 1;
                    MT.Keys.Add(newKey);
                }
            }
            TS.Label = "Locating important object";
            TS.Progress = 0.1f;


            if (MT.Altars.Count > 0) res.AltarTile = MT.Altars[RNG.Next(MT.Altars.Count)];
            else {
                //No altar set... Turn one key into the Altar...
                Tile Alt = MT.Keys[RNG.Next(MT.Keys.Count)];
                Alt.TileMode = 2;
                MT.Keys.Remove(Alt);
                res.AltarTile = Alt;
            }

            Room AltarRoom = (Room)res.AltarTile.Group;

            TS.Label = "Mapping potential routes";
            TS.Progress = 0.15f;

            RouteCollection RC = new RouteCollection(RM.Passages, AltarRoom);

            if (RC.DroppedPassages.Count > 0) {
                List<Tile.TileGroup> DroppedGroups = new List<Tile.TileGroup>();
                foreach (Passage P in RC.DroppedPassages) {
                    if (DroppedGroups.Contains(P.From) == false) DroppedGroups.Add(P.From);
                    if (DroppedGroups.Contains(P.To) == false) DroppedGroups.Add(P.To);
                    DroppedGroups.Add(P);
                }

                foreach (Tile.TileGroup G in DroppedGroups) {
                    foreach (Tile T in G.Tiles) {
                        T.TileMode = -1;
                        if (MT.Keys.Contains(T)) MT.Keys.Remove(T);
                        if (MT.Spawns.Contains(T)) MT.Spawns.Remove(T);
                    }
                }
            }
            TS.Label = "Eliminating unreacable rooms";
            TS.Progress = 0.35f;


            if (MT.Spawns.Count > 0) res.SpawnTile = MT.Spawns[RNG.Next(MT.Spawns.Count)];
            else {
                Tile[] free = AltarRoom.FreeTiles();
                if (free.Length == 0) free = res.AltarTile.Neighbors;
                res.SpawnTile = free[RNG.Next(free.Length)];
            }
            TS.Label = "Placing Blockages on non-essential routes";
            TS.Progress = 0.4f;

            while (RC.NonEssentials() > 0) {
                //for (int i = 0; i < 10; i++) {

                Passage Rem = RC.PickNonEssential(RNG);
                Tile Block = Rem.PickRandomTile(RNG);
                Block.Blockage = RNG.Next(MT.Keys.Count + 1);
                Debug.Log("Added Blockage for key: " + Block.Blockage);
                RC.ExcludePassage(Rem);
                TS.Progress = (1 + TS.Progress)/2;

            }
            TS.Label = "Almost there...";
            TS.Progress = 1f;
            if (res.SpawnTile.Blockage == 0) res.SpawnTile.Blockage = 1;

            res.Tiles = MT.AllTiles.ToArray();
            res.KeyTiles = MT.Keys.ToArray();

            Debug.Log("Done generating result");
        }, () => {
            if (Callback != null) Callback(res);
        });       

        return Sync;

    }


    class TileMap {
        public Tile[,] Tiles;
        public List<Tile> AllTiles;
        public List<Tile> Altars;
        public List<Tile> Spawns;
        public List<Tile> Keys;

        public TileMap(Texture2D Map, Color BGKey, Color GemKey, Color AltarKey, Color StartKey) {

            int Width = Map.width - 1;
            int Height = Map.height - 1;
            Tiles = new Tile[Width, Height];
            AllTiles = new List<Tile>();
            Altars = new List<Tile>();
            Spawns = new List<Tile>();
            Keys = new List<Tile>();
            //Rad texture into map;
            for (int i = 1; i < Width; i++) {
                for (int j = 1; j < Height; j++) {
                    Color S = Map.GetPixel(i, j);
                    if (S != BGKey) {
                        Tile curTile = new Tile() {
                            Up = Tiles[i, j - 1],
                            Left = Tiles[i - 1, j],
                            TileMode = (S == GemKey) ? 1 : (S == AltarKey) ? 2 : (S == StartKey) ? 3 : 0,
                            X = i,
                            Y = j
                        };
                        if (curTile.TileMode == 1) Keys.Add(curTile);
                        if (curTile.TileMode == 2) Altars.Add(curTile);
                        if (curTile.TileMode == 3) Spawns.Add(curTile);

                        if (curTile.Left != null) curTile.Left.Right = curTile;
                        if (curTile.Up != null) curTile.Up.Down = curTile;
                        Tiles[i, j] = curTile;
                        AllTiles.Add(curTile);
                    }
                }
            }
        }
    }

    public class Room : Tile.TileGroup {
        //public bool IsImportat;
        public Tile[] FreeTiles() {
            return Tiles.Where((T) => { return T.TileMode == 0; }).ToArray();
        }
        public bool ConaisAltar() {
            return Tiles.Where((T) => { return T.TileMode == 2; }).Count() > 0;
        }
        public bool ConaisGem() {
            return Tiles.Where((T) => { return T.TileMode == 1; }).Count() > 0;
        }
        public bool ConaisStart() {
            return Tiles.Where((T) => { return T.TileMode == 3; }).Count() > 0;
        }
        public bool ConaisSpecial() {
            return Tiles.Where((T) => { return T.TileMode != 0; }).Count() > 0;
        }
    }
    public class Passage : Tile.TileGroup {
        public Room From;
        public Room To;

    }

    class RoomManager {
        
        



        public Room[] Halls;
        public Passage[] Passages;

        public RoomManager(List<Tile> TileSet) {
            List<Tile> Type1 = new List<Tile>();
            List<Tile> Type0 = new List<Tile>();
            foreach (Tile T in TileSet) {
                if (T.NeighborsCount > 2 || T.NeighborsCount == 1 || T.TileMode != 0) {
                    Type1.Add(T);
                    T.GroupType = 1;
                }
                else if (T.NeighborsCount == 0) T.GroupType = -1;
                else {
                    T.GroupType = 0;
                    Type0.Add(T);
                }
            }

            List<Room> halls = new List<Room>();

            while (Type1.Count > 0) {
                halls.Add(AssignHall(Type1[0], Type1));
            }

            foreach (Tile T in Type0) {
                if (IsHallCorner(T)) Type1.Add(T);
            }
            //Removing Hall corners from Corridor lists;
            foreach (Tile T in Type1) {
                Type0.Remove(T);
            }

            List<Passage> passages = new List<Passage>();

            //Assigning Passages to groups;
            while (Type0.Count > 0) {
                passages.Add(AssignCorridor(Type0[0], Type0));
            }

            Halls = halls.ToArray();
            Passages = passages.ToArray();
        }
        Room AssignHall(Tile T, List<Tile> Exclude) {
            Room myHall = new Room();
            ExpandGroup(T, myHall, 1, Exclude);
            //myHall.IsImportat = false;
            //foreach (Tile I in myHall.Tiles) {
            //    if (I.TileMode != 0) myHall.IsImportat = true;
            //}
            return myHall;
        }

        Passage AssignCorridor(Tile T, List<Tile> Exclude) {
            Passage myPassage = new Passage();
            ExpandGroup(T, myPassage, 0, Exclude);

            List<Room> Conections = new List<Room>();
            //Debug.Log("Passage contains tiles: "+myPassage.Tiles.Length);
            foreach (Tile I in myPassage.Tiles) {
                if (I.Up != null && I.Up.GroupType != 0) Conections.Add((Room)I.Up.Group);
                if (I.Down != null && I.Down.GroupType != 0) Conections.Add((Room)I.Down.Group);
                if (I.Left != null && I.Left.GroupType != 0) Conections.Add((Room)I.Left.Group);
                if (I.Right != null && I.Right.GroupType != 0) Conections.Add((Room)I.Right.Group);
            }
            if (Conections.Count == 0) {
                //Debug.Log("Loop Located");
            }
            else if (Conections.Count != 2) {
                Debug.LogError("Something hairy going on...");
            }
            else {
                myPassage.From = Conections[0];
                myPassage.To = Conections[1];
            }
            return myPassage;
        }
        void ExpandGroup(Tile T, Tile.TileGroup G, int GroupType, List<Tile> Exclude) {
            //if (GroupType ==0) { Debug.Log("Expanding Corridor to : " + T.X + "x" + T.Y + " (" + T.GroupType + ")"); }
            if (T.Group != null) return; // Already in group!
            if (T.GroupType != GroupType) return; //Incompatible group type!
            G.AddTile(T);//
            Exclude.Remove(T);
            if (T.Up != null) ExpandGroup(T.Up, G, GroupType, Exclude);
            if (T.Down != null) ExpandGroup(T.Down, G, GroupType, Exclude);
            if (T.Left != null) ExpandGroup(T.Left, G, GroupType, Exclude);
            if (T.Right != null) ExpandGroup(T.Right, G, GroupType, Exclude);
        }
        bool IsHallCorner(Tile T) {
            Tile N1 = T.Up != null ? T.Up : T.Down != null ? T.Down : T.Left;
            Tile N2 = T.Right != null ? T.Right : T.Left != null ? T.Left : T.Down;
            if (N1 == null || N2 == null) return false; //Should neve occure in normal execution
            if (N1.Group != null && N1.Group == N2.Group) {
                T.Group = N1.Group;
                T.GroupType = 1;
                return true;
            }
            return false;
        }
    }

    class RouteCollection {
        //public Room[] ReachableRooms;
        public List<Passage> UsedPassages;
        public List<Passage> DroppedPassages;
        public Passage[] Mandatory;
        public Room[] Important;
        List<Route> Routes;
        public RouteCollection(IEnumerable<Passage> Passages, Room Start ) {
            Routes = new List<Route>();
            Stack<Route> NewRoutes = new Stack<Route>();
            Stack<Route> PrevRoutes;

            List<Room> imps = new List<Room>();
            List<Passage> passages = new List<Passage>();


            Route Root = new Route() { Passages = new List<Passage>(), VisitedNodes = new List<Room>() { Start }, FinalDest = Start };
            //int newlyAdded = 0;
            foreach (Passage P in Passages) {
                Route New = Root.AppendPassage(P);
                if (New != null) {
                    //Routes.Add(New);
                    //newlyAdded++;
                    NewRoutes.Push(New);
                    if (passages.Contains(P) == false) passages.Add(P);
                }
            }
            while (NewRoutes.Count != 0) {
                //for (int i = 0; i < 25; i++) {
                PrevRoutes = NewRoutes;
                NewRoutes = new Stack<Route>();
                while (PrevRoutes.Count>0) {
                    //foreach (Route R in PrevAdded) {
                    Route R = PrevRoutes.Pop();
                    Room Ro = R.FinalDest;
                    if (Ro.ConaisSpecial()) {
                        Routes.Add(R);
                        if (imps.Contains(Ro) == false) imps.Add(Ro);
                    }
                    foreach (Passage P in Passages) {
                        Route New = R.AppendPassage(P);
                        if (New != null) {
                            //Routes.Add(New);
                            //newlyAdded++;
                            NewRoutes.Push(New);
                            if (passages.Contains(P) == false) passages.Add(P);
                        }
                        else {
                        }
                    }
                }                
            }

            
            //this.Routes = new List<Route>(Routes);
            //    Recount();
            //}

            //void Recount() {
            //List<Room> rooms = new List<Room>();
            
            
            //ReachableRooms = rooms.ToArray();
            UsedPassages = passages;//.ToArray();
            DroppedPassages = Passages.Except(UsedPassages).ToList();
            Important = imps.ToArray();
            CountMandatory();
        }


        


        void CountMandatory() {
            List<Passage> man = new List<Passage>();
            foreach (Room Ro in Important) {
                List<Passage> tmp = new List<Passage>(UsedPassages.Except(man));
                Route[] Rs = Routes.Where((R) => { return R.FinalDest == Ro; }).ToArray();
                foreach (Route R in Rs) {
                    IEnumerable<Passage> missing = tmp.Except(R.Passages).ToArray();
                    foreach (Passage P in missing) tmp.Remove(P);
                }
                foreach (Passage P in tmp) {
                    man.Add(P);
                }
            }
            Mandatory = man.ToArray();
        }
        public int NonEssentials() {
            return UsedPassages.Except(Mandatory).Count();
        }
        public Passage PickNonEssential(System.Random RNG) {
            Passage[] NonEssentials = UsedPassages.Except(Mandatory).ToArray();
            return NonEssentials[RNG.Next(NonEssentials.Length)];
        }
        public void ExcludePassage(Passage P) {
            IEnumerable<Route> Removals = Routes.Where((R) => { return R.Passages.Contains(P); }).ToArray();
            foreach (Route R in Removals) {
                Routes.Remove(R);
            }
            UsedPassages.Remove(P);
            CountMandatory();
        }

        class Route {
            public List<Passage> Passages;
            public Room FinalDest;
            public List<Room> VisitedNodes;
            public bool CanAdd(Passage P) {
                if (P.From == FinalDest) {
                    return (VisitedNodes.Contains(P.To) == false);
                }
                else if (P.To == FinalDest) {
                    return (VisitedNodes.Contains(P.From) == false);
                }
                return false;
            }
            public Route AppendPassage(Passage P) {
                if (!CanAdd(P)) return null;
                Route res = new Route() {
                    Passages = new List<Passage>(Passages),
                    VisitedNodes = new List<Room>(VisitedNodes)
                };
                if (P.From == FinalDest) res.FinalDest = P.To;
                else if (P.To == FinalDest) res.FinalDest = P.From;
                res.Passages.Add(P);
                res.VisitedNodes.Add(res.FinalDest);
                return res;
            }
        }
    }

}
